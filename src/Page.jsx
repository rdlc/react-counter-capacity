import React, { useState } from 'react'

import Counter from './components/Counter'
import Number from './components/Number'
import Input from './components/Input'

import { Container } from './Page.style'

function Page() {

    const [initial, setInitial] = useState(0)
    const [max, setMax] = useState('')

    const handleAdd = () => {
        if ( initial == max ) {
            return
        }

        if ( initial < 999999999 ) {
            setInitial(initial + 1);
        }
    }

    const handleSubtract = () => {
        if ( initial != 0 ) {
            setInitial(initial - 1);
        }
    }

    const handleChange = (event) => {
        if ( initial > event.target.value ){
            setInitial(event.target.value)
        }
        if (event.target.value == '') {
            setInitial(0)
        }
        setMax(event.target.value);
    }

    return (
        <>
            <Input
                type="text"
                placeholder="Ejm. 40"    
                onChange={ handleChange }
                value = { max }
            > Ingresa el numero maximo: </Input>
            <Container>
                <Number> { initial } </Number>
                <Counter onClick={ handleAdd } color='#a6f0c6'> +1 </Counter>
                <Counter onClick={ handleSubtract } color='#eb596e'> -1 </Counter>
            </Container>
        </>
    )
}

export default Page
