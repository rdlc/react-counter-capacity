import React from 'react'
import PropTypes from 'prop-types'
import { Button } from './Counter.style'

const Counter = ({ children, ...props }) => {
    return (
        <>
            <Button {...props}>
                 <> {children} </>     
            </Button>
        </>
    )
}

Counter.propTypes = {
    children: PropTypes.string.isRequired,
    color: PropTypes.oneOf(['#a6f0c6', '#eb596e'])
}

Counter.defaultProps = {
    color: '#a6f0c6'
}
  

export default Counter
