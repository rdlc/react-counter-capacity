import styled from 'styled-components';

export const Button = styled.button`
    background-color: ${props => props.color};
    border: transparent;
    border-radius: 6px;
    cursor: pointer;
    color: black;
    font-size: 50px;
    padding: 10px;
    width: 50%;
    height: 500px;
    font-weight: bold; 
    font-family: 'Muli', sans-serif;
`;
