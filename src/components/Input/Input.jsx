import React from 'react'
import PropTypes from 'prop-types'
import { InputContainer, Title } from './Input.style'


function Input( { children, ...props } ) {
    return (
        <>
          <Title> { children }  </Title>
          <InputContainer {...props}>
          </InputContainer>  
        </>
    )
}

Input.propTypes = {
    children: PropTypes.string.isRequired
}

export default Input

