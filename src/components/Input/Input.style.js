import styled from "styled-components";

export const InputContainer = styled.input`
    padding: 8px;
    margin: 30px 10px;
`

export const Title = styled.label`
    font-size: 15px;
    font-family: 'Muli', sans-serif;
    margin-left: 20px;
`