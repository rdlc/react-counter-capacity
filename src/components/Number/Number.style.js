import styled from 'styled-components';

export const Title = styled.h1`
    color: black;
    position: absolute;
    background-color: white;
    font-size: 50px;
    padding: 10px 5%;
    font-weight: bold; 
    font-family: 'Muli', sans-serif;
`