import React from 'react'
import PropTypes from 'prop-types'
import { Title } from './Number.style'

function Number( { children } ) {
    return (
        <>
            <Title>
                { children }
            </Title>
        </>
    )
}

export default Number
