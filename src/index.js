import React from 'react';
import ReactDOM from 'react-dom';
import Page from './Page'

const divRoot = document.querySelector('#root');

ReactDOM.render( <Page></Page>  , divRoot );
